# Vim

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with vim](#setup)
    * [What vim affects](#what-vim-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with vim](#beginning-with-vim)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
6. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module configures vim on Debian based systems.

## Module Description

This module manages installation and configuration of vim on Debian based systems.

## Setup

### What vim affects

* Insall vim and manages /etc/vim/vimrc.local

### Setup Requirements

* None.

### Beginning with vim

See [Usage](#usage)

## Usage

### Using default values

	class { '::vim': }

### Using available parameters

	class { '::vim':
		syntax => true,
		background => 'light' }

## Reference

### Public Classes

* vim: Main class, includes all other classes.

### Private Classes

* vim::install: Handles the packages.
* vim::config: Handles the configuration files.

### Parameters

#### `syntax`

Enable/disable syntax highlightning in vim.

#### `background`

Sets the background scheme for vim (possible values: `dark` and `light`).

## Limitations

This module has been built on and tested against Puppet 4.

The module has beent tested on Debian Jessie.
