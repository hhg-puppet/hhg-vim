# == Class: vim
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class vim
(
	$syntax = true,
	$background = dark
)

{
	# Check parameters
	validate_bool($syntax)
	validate_string($background)
	# TODO check supported background types
	
	# Install vim
	include vim::install
	
	# Configure vimrc.local
	include vim::config
}
