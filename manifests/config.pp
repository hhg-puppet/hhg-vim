# == Class: vim::config
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class vim::config inherits vim {
	
	file { '/etc/vim/vimrc.local':
		owner => 'root',
		group => 'root',
		mode => '644',
		content => template('vim/vimrc.local.erb')
	}
	
}
