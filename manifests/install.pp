# == Class: vim::install
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class vim::install inherits vim {
	
	package { 'vim':
		ensure => 'installed'
	}
	
}
